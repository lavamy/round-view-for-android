package com.uphave.ui.components;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;

import com.uphave.ui.R;

public class TypefaceTextView extends TextView {
    private String mFont;

    public TypefaceTextView(Context context) {
        super(context);
        init(null, 0, 0);
    }

    private void init(AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        if (attrs == null) {
            return;
        }

        final TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.TypefaceTextView, defStyleAttr, defStyleRes);
        setFont(a.getString(R.styleable.TypefaceTextView_font));
        a.recycle();

    }

    public TypefaceTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0, 0);
    }

    public TypefaceTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs, defStyleAttr, 0);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public TypefaceTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs, defStyleAttr, defStyleRes);
    }

    public String getFont() {
        return mFont;
    }

    public void setFont(String font) {
        mFont = font;

        if (!TextUtils.isEmpty(mFont)) {
            Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), mFont);
            setTypeface(typeface);
        }
    }
}
